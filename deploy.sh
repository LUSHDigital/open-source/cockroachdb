#!/bin/bash

# Grab variables from our .env file
source .env

# Check gcloud is installed.
command -v gcloud >/dev/null 2>&1 || { echo "This script requires gcloud, but it is not installed.  Aborting." >&2; exit 1; }

gcloud compute --project=$GCP_PROJECT_R1 addresses create $GCP_PROJECT_R1-cockroachdb-0 --region=$GCP_REGION_R1
gcloud compute --project=$GCP_PROJECT_R1 addresses create $GCP_PROJECT_R1-cockroachdb-1 --region=$GCP_REGION_R1
gcloud compute --project=$GCP_PROJECT_R1 addresses create $GCP_PROJECT_R1-cockroachdb-2 --region=$GCP_REGION_R1
gcloud compute --project=$GCP_PROJECT_R2 addresses create $GCP_PROJECT_R2-cockroachdb-0 --region=$GCP_REGION_R2
gcloud compute --project=$GCP_PROJECT_R2 addresses create $GCP_PROJECT_R2-cockroachdb-1 --region=$GCP_REGION_R2
gcloud compute --project=$GCP_PROJECT_R2 addresses create $GCP_PROJECT_R2-cockroachdb-2 --region=$GCP_REGION_R2
