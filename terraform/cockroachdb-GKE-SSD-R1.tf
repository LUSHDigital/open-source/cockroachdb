resource "google_container_cluster" "primary" {
  project = "${var.GCP_PROJECT_R1}"
  zone = "${var.GCP_ZONE_R1}"
  name = "${var.GCP_CLUSTER_R1}"
  initial_node_count = 3
  machine_type  = "${var.GCP_MACHINE_TYPE}"
  node_version = "${var.GCP_NODE_VERSION}"

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

# The following outputs allow authentication and connectivity to the GKE Cluster.
output "client_certificate" {
  value = "${google_container_cluster.primary.master_auth.0.client_certificate}"
}

output "client_key" {
  value = "${google_container_cluster.primary.master_auth.0.client_key}"
}

output "cluster_ca_certificate" {
  value = "${google_container_cluster.primary.master_auth.0.cluster_ca_certificate}"
}

resource "google_container_cluster" "primary" {
  project = "${var.GCP_PROJECT_R2}"
  zone = "${var.GCP_ZONE_R2}"
  name = "${var.GCP_CLUSTER_R2}"
  initial_node_count = 3
  machine_type  = "${var.GCP_MACHINE_TYPE}"
  node_version = "${var.GCP_NODE_VERSION}"

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

# The following outputs allow authentication and connectivity to the GKE Cluster.
output "client_certificate" {
  value = "${google_container_cluster.primary.master_auth.0.client_certificate}"
}

output "client_key" {
  value = "${google_container_cluster.primary.master_auth.0.client_key}"
}

output "cluster_ca_certificate" {
  value = "${google_container_cluster.primary.master_auth.0.cluster_ca_certificate}"
}
