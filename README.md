# Cockroachdb GKE

This repository is an experimental attempt to cluster Cockroachdb hosted on GKE across multiple Google Cloud regions.

# Requirements

* gcloud

The gcloud command line tool. [https://cloud.google.com/pubsub/docs/quickstart-cli](https://cloud.google.com/pubsub/docs/quickstart-cli)

* GCP Projects, Billing and gke api enabled

You will need two GCP projects already created with billing set-up and the gke api enabled.

* kubectl

You can obtain `kubectl` using the gcloud command: `gcloud components install kubectl`

* Helm

You will need Helm for Kubernetes. [https://github.com/kubernetes/helm](https://github.com/kubernetes/helm)

* CockroachDB (needed for secure deployment only)

Install CockroachDB on your local machine, if you haven't already. [https://www.cockroachlabs.com/docs/stable/install-cockroachdb.html](https://www.cockroachlabs.com/docs/stable/install-cockroachdb.html)

# Insecure Mode

### Set your variables

Set the appropriate variables for your environment.

A helper script has been supplied to make this quicker, copy `set-variables.sh.default` to `set-variables.sh` edit and run:
```
. ./set-variables.sh
```

### Reserve IPs

We'll need external IPs for each Pod so CockroachDB can communicate across clusters, we will create six, three for each GKE cluster.

```
gcloud compute --project=$GCP_PROJECT_R1 addresses create $GCP_PROJECT_R1-cockroachdb-0 --region=$GCP_REGION_R1

gcloud compute --project=$GCP_PROJECT_R1 addresses create $GCP_PROJECT_R1-cockroachdb-1 --region=$GCP_REGION_R1

gcloud compute --project=$GCP_PROJECT_R1 addresses create $GCP_PROJECT_R1-cockroachdb-2 --region=$GCP_REGION_R1

*Repeat the process for Region Two:*

gcloud compute --project=$GCP_PROJECT_R2 addresses create $GCP_PROJECT_R2-cockroachdb-0 --region=$GCP_REGION_R2

gcloud compute --project=$GCP_PROJECT_R2 addresses create $GCP_PROJECT_R2-cockroachdb-1 --region=$GCP_REGION_R2

gcloud compute --project=$GCP_PROJECT_R2 addresses create $GCP_PROJECT_R2-cockroachdb-2 --region=$GCP_REGION_R2
```

### Create Helm values file

Duplicate `helm/cockroachdb-GKE/values.yaml` twice so you have a values file for each of your two regions. For instance create a `helm/cockroachdb-GKE/values-uk.yaml` and `helm/cockroachdb-GKE/values-jp.yaml` file.

Edit these duplicates by adding in the IPs you generated in the previous step, making sure to enter the IPs for the right region for the `CurrentRegion:` values. You can view these IPs using `gcloud compute addresses list --project=$GCP_PROJECT_R1` and `gcloud compute addresses list --project=$GCP_PROJECT_R2`.

Also change `Region.alpha-2` to a unique code for each region, eg. `"uk"` or `"jp"`. You can find a list of alpha-2 region codes here: [https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements)

### Create your clusters

Create your cluster, note we have set `--cluster-version` to `1.8.6-gke.0` so we have access to RBAC groups.

```
gcloud beta container --project "$GCP_PROJECT_R1" clusters create "$GCP_CLUSTER_R1" --zone "$GCP_ZONE_R1" --username "admin" --cluster-version "1.8.8-gke.0" --machine-type "custom-8-16384" --image-type "COS" --disk-size "100" --scopes "https://www.googleapis.com/auth/cloud-platform" --num-nodes "3" --network "default" --enable-cloud-logging --enable-cloud-monitoring --subnetwork "default" --enable-autoscaling --min-nodes "3" --max-nodes "20"
```

*Repeat the process for Region Two:*

```
gcloud beta container --project "$GCP_PROJECT_R2" clusters create "$GCP_CLUSTER_R2" --zone "$GCP_ZONE_R2" --username "admin" --cluster-version "1.8.8-gke.0" --machine-type "custom-8-16384" --image-type "COS" --disk-size "100" --scopes "https://www.googleapis.com/auth/cloud-platform" --num-nodes "3" --network "default" --enable-cloud-logging --enable-cloud-monitoring --subnetwork "default" --enable-autoscaling --min-nodes "3" --max-nodes "20"
```

### Deploy Helm charts

Set kubectl context to your first cluster:
```
gcloud container clusters get-credentials $GCP_CLUSTER_R1 --zone $GCP_ZONE_R1 --project $GCP_PROJECT_R1
```

To use Helm with newer Kubernetes clusters that include RBAC, you will need to create a user with admin access for tiller.
```
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
```

Initiate Helm:
```
helm init
```

Edit the Helm deployment to include you RBAC changes and reinitialise Helm
```
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
helm init
```

Install your Helm chart referencing the values file for your first region:
```
helm install -f helm/cockroachdb-GKE/values-uk.yaml helm/cockroachdb-GKE
```

*Repeat the process for Region Two:*

Set kubectl context to your second cluster:
```
gcloud container clusters get-credentials $GCP_CLUSTER_R2 --zone $GCP_ZONE_R2 --project $GCP_PROJECT_R2
```

To use Helm with newer Kubernetes clusters that include RBAC, you will need to create a user with admin access for tiller.
```
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
```

Initiate Helm:
```
helm init
```

Edit the Helm deployment to include you RBAC changes and reinitialise Helm
```
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
helm init
```

Install your Helm chart referencing the values file for your second region:
```
helm install -f helm/cockroachdb-GKE/values-jp.yaml helm/cockroachdb-GKE
```

### Initialise CockroachDB cluster

Edit `init/cluster-init.yaml` changing `"--host=123.456.789.10"` to any of the static IPs you created earlier.

Apply cluster-init.yaml:
```
kubectl apply -f init/cluster-init-secure.yaml
```

Check to see if the job complete successfully:
```
kubectl get jobs
```

```
NAME           DESIRED   SUCCESSFUL   AGE
cluster-init   1         1            1m
```

*You're all done!*

### Access the cluster's Admin UI

Port-forward from your local machine to one of the pods:
```
kubectl port-forward uk-cockroachdb-0 8080
```

Go to https://localhost:8080

You could also try deleting pods to simulate failure.

# Secure Mode

### Set your variables

Set the appropriate variables for your environment.

A helper script has been supplied to make this quicker, copy `set-variables.sh.default` to `set-variables.sh` edit and run:
```
. ./set-variables.sh
```

### Reserve IPs

We'll need external IPs for each Pod so CockroachDB can communicate across clusters, we will create six, three for each GKE cluster.

```
gcloud compute --project=$GCP_PROJECT_R1 addresses create $GCP_PROJECT_R1-cockroachdb-0 --region=$GCP_REGION_R1

gcloud compute --project=$GCP_PROJECT_R1 addresses create $GCP_PROJECT_R1-cockroachdb-1 --region=$GCP_REGION_R1

gcloud compute --project=$GCP_PROJECT_R1 addresses create $GCP_PROJECT_R1-cockroachdb-2 --region=$GCP_REGION_R1

*Repeat the process for Region Two:*

gcloud compute --project=$GCP_PROJECT_R2 addresses create $GCP_PROJECT_R2-cockroachdb-0 --region=$GCP_REGION_R2

gcloud compute --project=$GCP_PROJECT_R2 addresses create $GCP_PROJECT_R2-cockroachdb-1 --region=$GCP_REGION_R2

gcloud compute --project=$GCP_PROJECT_R2 addresses create $GCP_PROJECT_R2-cockroachdb-2 --region=$GCP_REGION_R2
```

### Create Helm values file

Duplicate `helm/cockroachdb-GKE/values.yaml` twice so you have a values file for each of your two regions. For instance create a `helm/cockroachdb-GKE/values-uk.yaml` and `helm/cockroachdb-GKE/values-jp.yaml` file.

Edit these duplicates by adding in the IPs you generated in the previous step, making sure to enter the IPs for the right region for the `CurrentRegion:` values. You can view these IPs using `gcloud compute addresses list --project=$GCP_PROJECT_R1` and `gcloud compute addresses list --project=$GCP_PROJECT_R2`.

Also change `Region.alpha-2` to a unique code for each region, eg. `"uk"` or `"jp"`. You can find a list of alpha-2 region codes here: [https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements)

Switch the default value of `secure: false` to `secure: true` for each of your values files.

Copy the output and set it as the value for `ca` in both your `helm/cockroachdb-GKE/values-*.yaml` files.

### Create your clusters

Create your cluster, note we have set `--cluster-version` to `1.8.6-gke.0` so we have access to RBAC groups.

```
gcloud beta container --project "$GCP_PROJECT_R1" clusters create "$GCP_CLUSTER_R1" --zone "$GCP_ZONE_R1" --username "admin" --cluster-version "1.8.7-gke.1" --machine-type "n1-standard-1" --image-type "COS" --disk-size "100" --scopes "https://www.googleapis.com/auth/cloud-platform" --num-nodes "3" --network "default" --enable-cloud-logging --enable-cloud-monitoring --subnetwork "default" --enable-autoscaling --min-nodes "3" --max-nodes "20"
```

*Repeat the process for Region Two:*

```
gcloud beta container --project "$GCP_PROJECT_R2" clusters create "$GCP_CLUSTER_R2" --zone "$GCP_ZONE_R2" --username "admin" --cluster-version "1.8.7-gke.1" --machine-type "n1-standard-1" --image-type "COS" --disk-size "100" --scopes "https://www.googleapis.com/auth/cloud-platform" --num-nodes "3" --network "default" --enable-cloud-logging --enable-cloud-monitoring --subnetwork "default" --enable-autoscaling --min-nodes "3" --max-nodes "20"
```

### Copy GKE certificates

We will copy out the GKE cluster's .crt certs and then combine them so out CockroachDB nodes can reference them.

Set kubectl context to your first cluster:
```
gcloud container clusters get-credentials $GCP_CLUSTER_R1 --zone $GCP_ZONE_R1 --project $GCP_PROJECT_R1
```

list the cluster secrets:
```
kubectl get secret
```

You will see a single secret with a name matching `default-token-*`. You will need to copy out the ca data from this secret:
```
kubectl get secret default-token-AB12C -o yaml
```

Copy out the Base64 encoded string from `ca.crt:` and Base64 decode it using a either the `base64` command line tool or a website like [base64decode.org](https://www.base64decode.org/), you should see a cert begging and ending with the familiar `-----BEGIN CERTIFICATE-----` and `-----END CERTIFICATE-----` header and footer. Copy the cert and a whole including header and footer and save somewhere later use.

*Repeat the process for Region Two:*

et kubectl context to your second cluster:
```
gcloud container clusters get-credentials $GCP_CLUSTER_R2 --zone $GCP_ZONE_R2 --project $GCP_PROJECT_R2
```

list the cluster secrets:
```
kubectl get secret
```

You will see a single secret with a name matching `default-token-*`. You will need to copy out the ca data from this secret:
```
kubectl get secret default-token-AB12C -o yaml
```

Copy out the Base64 encoded string from `ca.crt:` and Base64 decode it using a either the `base64` command line tool or a website like [base64decode.org](https://www.base64decode.org/), you should see a cert begging and ending with the familiar `-----BEGIN CERTIFICATE-----` and `-----END CERTIFICATE-----` header and footer.

Copy the cert and a whole including header and footer and combine it with the cert from your first cluster by pasting it directly bellow so that the line directly after the `-----END CERTIFICATE-----` of the first cert is the `-----BEGIN CERTIFICATE-----` for the second cert. Now copy these two certs as a whole and Base64 encode them, take the resulting string and paste it into both your Helm values files `helm/cockroachdb-GKE/values-*.yaml` for the value `ca: ""`.

### Deploy Helm charts

Set kubectl context to your first cluster:
```
gcloud container clusters get-credentials $GCP_CLUSTER_R1 --zone $GCP_ZONE_R1 --project $GCP_PROJECT_R1
```

To use Helm with newer Kubernetes clusters that include RBAC, you will need to create a user with admin access for tiller.
```
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
```

Initiate Helm:
```
helm init
```

Edit the Helm deployment to include you RBAC changes and reinitialise Helm
```
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
helm init
```

Install your Helm chart referencing the values file for your first region:
```
helm install -f helm/cockroachdb-GKE/values-uk.yaml helm/cockroachdb-GKE
```

Each pod will come up in order and ask for a "Certificate Signing Request" to be approved. You can check which certificates are waiting to be signed by watching:
```
kubectl get csr
```

Once a certificate is `pending` you can approve it, for example:
```
kubectl certificate approve default.node.uk-cockroachdb-0
```

Each of the three pods will do this in turn. Once all three certificates are approved you can move on to bringing up the nodes in the second cluster.

*Repeat the process for Region Two:*

Set kubectl context to your second cluster:
```
gcloud container clusters get-credentials $GCP_CLUSTER_R2 --zone $GCP_ZONE_R2 --project $GCP_PROJECT_R2
```

To use Helm with newer Kubernetes clusters that include RBAC, you will need to create a user with admin access for tiller.
```
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
```

Initiate Helm:
```
helm init
```

Edit the Helm deployment to include you RBAC changes and reinitialise Helm
```
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
helm init
```

Install your Helm chart referencing the values file for your second region:
```
helm install -f helm/cockroachdb-GKE/values-jp.yaml helm/cockroachdb-GKE
```

Each pod will come up in order and ask for a "Certificate Signing Request" to be approved. You can check which certificates are waiting to be signed by watching:
```
kubectl get csr
```

Once a certificate is `pending` you can approve it, for example:
```
kubectl certificate approve default.node.jp-cockroachdb-0
```

Each of the three pods will do this in turn. Once all three certificates are approved you can move on to initialising the cluster.

### Initialise CockroachDB cluster

Edit `init/cluster-init-secure.yaml` changing `"--host=123.456.789.10"` to any of the static IPs you created earlier.

Apply cluster-init.yaml:
```
kubectl apply -f init/cluster-init-secure.yaml
```

Check to see if the job complete successfully:
```
kubectl get jobs
```

```
NAME           DESIRED   SUCCESSFUL   AGE
cluster-init   1         1            1m
```

*You're all done!*

### Access the cluster's Admin UI

Port-forward from your local machine to one of the pods:
```
kubectl port-forward uk-cockroachdb-0 8080
```

Go to https://localhost:8080

### Test the cluster

Use `client/client-secure.yaml` to deploy an application Pod with access to your Cockroachdb cluster:

`kubectl create -f client/client-secure.yaml`

Jump into the application with:
`kubectl exec -it cockroachdb-client-secure -- ./cockroach sql --certs-dir=/cockroach-certs --host=cockroachdb-public`

*You can now write and excite SQL Queries against your Cockroachdb cluster*

use `\q` to exit.


You could also try deleting pods to simulate failure.

## Adding another secure region to an exsiting Cockroachdb Cluster

### Create project

Create another GCP project with Billing and gke api enabled.

### Update set-variables.sh

update set-variables with R3 values and re-run `. ./set-variables.sh`. eg:

```
# Region three:
export GCP_PROJECT_R3="your-region-three-gcp-project"
export GCP_ZONE_R3="your-region-three-gcp-zone"
export GCP_REGION_R3="your-region-three-gcp-region"
export GCP_CLUSTER_R3="your-region-three-gcp-cluster"
```

## Create IPs for your new region

```
gcloud compute --project=$GCP_PROJECT_R3 addresses create $GCP_PROJECT_R3-cockroachdb-0 --region=$GCP_REGION_R3

gcloud compute --project=$GCP_PROJECT_R3 addresses create $GCP_PROJECT_R3-cockroachdb-1 --region=$GCP_REGION_R3

gcloud compute --project=$GCP_PROJECT_R3 addresses create $GCP_PROJECT_R3-cockroachdb-2 --region=$GCP_REGION_R3
```

### Update Helm values files

Add the new IPs to the end of your `All:` strings in both existing values files and duplicate one values file to make a values file for your third region.

Edit the values file for your new region, making sure to set `Region.Alpha-2` and `CurrentRegion:`.

### Create a GKE cluster for your new region

```
gcloud beta container --project "$GCP_PROJECT_R3" clusters create "$GCP_CLUSTER_R3" --zone "$GCP_ZONE_R3" --username "admin" --cluster-version "1.8.7-gke.1" --machine-type "n1-standard-1" --image-type "COS" --disk-size "100" --scopes "https://www.googleapis.com/auth/cloud-platform" --num-nodes "3" --network "default" --enable-cloud-logging --enable-cloud-monitoring --subnetwork "default" --enable-autoscaling --min-nodes "3" --max-nodes "20"
```

### Update certs for all clusters

We will need to copy out the new GKE cluster's .crt cert and then combine it with the current certs, this will then need to be handed to both existing clusters and the new cluster.

Set kubectl context to your third cluster:
```
gcloud container clusters get-credentials $GCP_CLUSTER_R3 --zone $GCP_ZONE_R3 --project $GCP_PROJECT_R3
```

list the cluster secrets:
```
kubectl get secret
```

You will see a single secret with a name matching `default-token-*`. You will need to copy out the ca data from this secret:
```
kubectl get secret default-token-AB12C -o yaml
```

Copy out the Base64 encoded string from `ca.crt:` and Base64 decode it using a either the `base64` command line tool or a website like [base64decode.org](https://www.base64decode.org/), you should see a cert begging and ending with the familiar `-----BEGIN CERTIFICATE-----` and `-----END CERTIFICATE-----` header and footer. Copy the cert and a whole including header and footer and save somewhere later use.

Copy the base64 encoded value you current have in your values files for `ca:` and base64 decode it to reveal your two current certs, combine these with the cert from your third cluster by pasting it directly bellow so that the line directly after the `-----END CERTIFICATE-----` of the second cert is the `-----BEGIN CERTIFICATE-----` for the third cert. Now copy these three certs as a whole and Base64 encode them, take the resulting string and paste it into all three of your Helm values files `helm/cockroachdb-GKE/values-*.yaml` for the value `ca: ""`.

### Deploy Helm charts to new cluster

Now that your values files have been undated you can deploy to your new cluster and update the Helm instances running in your current clusters.

Ensure kubectl context is set to your third cluster:
```
gcloud container clusters get-credentials $GCP_CLUSTER_R3 --zone $GCP_ZONE_R3 --project $GCP_PROJECT_R3
```

To use Helm with newer Kubernetes clusters that include RBAC, you will need to create a user with admin access for tiller.
```
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
```

Initiate Helm:
```
helm init
```

Edit the Helm deployment to include you RBAC changes and reinitialise Helm
```
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
helm init
```

Install your Helm chart referencing the values file for your thrid region:
```
helm install -f helm/cockroachdb-GKE/values-us.yaml helm/cockroachdb-GKE
```

Each pod will come up in order and ask for a "Certificate Signing Request" to be approved. You can check which certificates are waiting to be signed by watching:
```
kubectl get csr
```

Once a certificate is `pending` you can approve it, for example:
```
kubectl certificate approve default.node.us-cockroachdb-0
```

Each of the three pods will do this in turn. Once all three certificates are approved you can move on to initialising the cluster.

### Re-Deploy Helm charts to old clusters

Set kubectl context to your first cluster:
```
gcloud container clusters get-credentials $GCP_CLUSTER_R1 --zone $GCP_ZONE_R1 --project $GCP_PROJECT_R1
```

List your Helm instances to find the running chart:
```
helm list
```

Upgrade the running chart with the new values you set by referencing the running chart, eg.:

```
helm upgrade -f helm/cockroachdb-GKE/values-uk.yaml elevated-echidna helm/cockroachdb-GKE
```

*Repeat the process for Region Two:*

Set kubectl context to your second cluster:
```
gcloud container clusters get-credentials $GCP_CLUSTER_R2 --zone $GCP_ZONE_R2 --project $GCP_PROJECT_R2
```

List your Helm instances to find the running chart:
```
helm list
```

Upgrade the running chart with the new values you set by referencing the running chart, eg.:

```
helm upgrade -f helm/cockroachdb-GKE/values-jp.yaml boisterous-duck helm/cockroachdb-GKE
```

*You're all done!*

The StatefulSet will take down pods one at a time in order, it may take a few minutes for all pods to go down and come back up with the new configuration.
